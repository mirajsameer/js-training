# Training material and Projects

Each Project is designed to take up at most 1 week.


# Assigment 1 - Get familiar with unit testing & common functions

## Goal

Develop, using a test driven approach, a small library of re-usable functions.
This is a pre-requisite for introducing a web framework such as React.

The point is to teach you:

1. How to setup unit testing and how to write proper tests
2. How to develop small functions and test as you go.

## Requirements

Implement the following functions using a test first approach( a reference implementation in lodash is given for example purposes):


1. Map - https://lodash.com/docs/4.17.4#map . Do NOT use the built in Array.map
2. Filter - https://lodash.com/docs/4.17.4#filter . Do NOT use the built in Array.filter
3. Difference - https://lodash.com/docs/4.17.4#difference
4. Flatten - https://lodash.com/docs/4.17.4#flatten
5. Uniq - https://lodash.com/docs/4.17.4#uniqBy
6. Every - https://lodash.com/docs/4.17.4#every
7. GroupBy - https://lodash.com/docs/4.17.4#groupBy
8. Reduce - https://lodash.com/docs/4.17.4#reduce . Do NOT use the built in Array.reduce
9. Remove - https://lodash.com/docs/4.17.4#remove
10. Compact - https://lodash.com/docs/4.17.4#compact

Increased difficulty functions (bonus points) :

1. https://lodash.com/docs/4.17.4#negate
2. https://lodash.com/docs/4.17.4#memoize
3. https://lodash.com/docs/4.17.4#throttle
4. https://lodash.com/docs/4.17.4#flowRight


I encourage you to try out the lodash version( click 'Run in Repl' ) to see how it behaves.

## How to

1. Clone this  ( `git clone git@gitlab.com:gcazaciuc/js-training.git` )
2. Setup Jest for unit testing ( https://facebook.github.io/jest/docs/en/getting-started.html )
3. Create a file named `utils.js` in `src` and put in there all of the functions.
4. Before implementing a function create the tests first( in `__tests__/util-spec.js`)

## Setting up unit testing


```console
npm i jest --dev
```

Make sure in `package.json` file that you have:

```js
"scripts": {
    "test": "jest --watch"
}
```

Run the unit tests: `npm run test`

Important: Do not use built-in Array.map, Array.filter functions but instead use for loops and re-use the functions you've already implemented previously. Do not use any other third party library

# Project 2 - Getting familiar with ES6

## Goal

Get the hang of the most important ES6 constructs.

## References

1. https://ponyfoo.com/articles/es6-modules-in-depth
2. https://ponyfoo.com/articles/es6-promises-in-depth
3. https://ponyfoo.com/articles/es6-classes-in-depth
4. https://ponyfoo.com/articles/es6-object-literal-features-in-depth
5. https://ponyfoo.com/articles/es6-spread-and-butter-in-depth
6. https://ponyfoo.com/articles/es6-destructuring-in-depth
7. https://ponyfoo.com/articles/es6-let-const-and-temporal-dead-zone-in-depth
8. https://ponyfoo.com/articles/es6-arrow-functions-in-depth
9. https://ponyfoo.com/articles/es6-template-strings-in-depth


## Requirements

Resolve the following ES6 katas( http://es6katas.org/ ) by fixing the tests but NOT changing the assertions:

* Promise
* Array
* Class
* Destructuring
* Object literal
* Template strings
* Arrow functions
* Block scope
* Rest operator
* Spread operator
* Default parameters
* Modules


# Project 3 - Taking it up a notch with Babel/Webpack

## Goal

So far, we've only fixed the tests for ES6 constructs while running in the browser.
Now it's time to learn about how you can transform ES6 to be runnable in all todays browsers(using Babel) and how to bundle together, in a single file, multiple sources(using Webpack)

## Requirements

Follow step by step an ensure that you understand how Babel and Webpack compilation is setup:

http://ccoenraets.github.io/es6-tutorial/

# Project 4 - Understanding Virtual DOM

The purpose of this is Project is to understand what is Virtual DOM, how it operates, how events are set, what is JSX.

At the end of this assigment you will be able to understand how all of the virtual DOM solutions actually work: React, Preact, Inferno, CycleJS and many many others.

Please go in small steps, exactly as outlined below so we can achieve the desired results:

## Pre-requisites

Make sure that you have a webpack config and babel config setup and functioning in your repo.

Virtual DOM is a way of describing **how** the DOM should look like. This description is handed off to a function that transforms it to the **real** DOM.

### Test framework setup

Make sure JEST is setup to compile sources using Babel, otherwise the tests will fail( see
http://facebook.github.io/jest/docs/en/getting-started.html#using-babel ).

Make sure that a container element is present in the DOM , in tests, so that `document.getElementById("app")` finds the DOM element it needs - otherwise, you've guessed it, the tests will fail.

To do that, I suggest to define a `beforeEach`(http://facebook.github.io/jest/docs/en/api.html#beforeeachfn) and `afterEach` hooks in `describe` functions.

In `beforeEach` create the container you need( eg using `document.createElement`), set any attributes on it(eg id) and add it to the DOM(eg on `body`).
In `afterEach` destroy the container and remove it from the DOM.




## Requirements

Please follow the steps in order, without skipping any, and create a GIT commit, with a meaningfull message at the end of each step.

### Step 1

Goal: create a function that can create DOM elements given the type of element that should be created.

1. Create a file in `src` called `vdom.js`. This is where our framework will be implemented
2. In it, create and export the following functions:
3. A function called `render(domDescription, container)` that can create a DOM node and return it.

```js
    const div = render({ type: 'div' }, document.getElementById("app"));
    const ul = render({ type: 'ul' }, document.querySelector("#test"));
```

The function params are as follows:

* domDescription  - An `object` having the property type, describing the type of element to create
* container - A DOM element where the create DOM node should be inserted


Should create and return a DOM node.

4. Add some tests for the `render` function
5. After this step COMMIT the result in GIT ( I should be able to review only this changes without issues)

### Step 2

Goal: Enhance `render` so that you can specify DOM attributes for the elements created by it.

1. Modify `render` to accept an object that also allows us to specify attributes for the elements being created.

```js
    const div = render({ 
                    type: 'div', 
                    attributes: {name: 'app', id: 'app'} 
                }, document.getElementById('app'));
    const ul = render({
        type: 'ul',
        attributes: null
    }, document.getElementById('app'))            
```

Make `attributes` an optional parameter(and default it to null).

2. Add tests to ensure the element can accept and create DOM nodes having the specified attibutes.
3. Commit the changes after the tests are added with a proper commit message

### Step 3

Goal: Enhance `render` so that you can  specify the text content of an element.

1. Modify `render` signature to accept a whole hierararchy of child components.

```js
    const div = render({
        type: 'div', 
        attributes: null, 
        children: 'This is the div content as text'
    }, document.getElementById('app'));
```

Make `children` an optional param and default it to the empty array( render should work properly even the key `children` is missing completely from DOM description object ).

2. Add tests for the render function
3. Commit the changes after the tests are added with a proper commit message

Hint/suggestion: Make use of el.textContent property to set the cintent when children is detected to be a string( do a type check first )

### Step 4

Goal: Make it easier to create DOM description object.

While writing
```js
{
        type: 'div', 
        attributes: null, 
        children: 'This is the div content as text'
}
```
 is not a huge deal, we need to make it easier to create DOM description objects.

#### Requirements

 1. Create a function called `createElement(type, [attributes], [children])` that returns objects of the above shape.
 
 Respect the following restrictions:
 - type is always required and is of type string
 - attributes is optional. Is either an `object` or `null`.Defaults to value `null`.
 - children is optional. Is either a string or an array of objects of type `{type: string, attributes: null | object, children: string| array }`. Defaults to empty array.

2. Add tests for `createElement` exercising all the situations.
3. Commit the changes.

### Step 5

Goal: Add support to specify `children` as elements besides supporting text content.

Enable `children` to also accept an array of objects besides `string` and `null`;
Make use of `createElement` helper function.

```js
/*
     * The below will generate an el to be a DOM element like 
     * <ul name="elWithChildren">
     *    <li id="firstLi">Li text content</li>
     *    <li id="secondLi"></li> 
     *    <li id="thirdLi"><div></div></li> 
     * </ul> 
     */
    const domDescription = createElement('ul', {name: 'elWithChildren'}, [
        createElement('li', {id: 'firstLi'}, 'Li text content'),
        createElement('li', {id: 'secondLi'}, null),
        createElement('li', {id: 'thirdLi'}, [
            createElement('div')
        ]),
    ]);
    const ul = render(domDescription, document.getElementById('app'));
```

2. Add tests exercising as many alternatives as possible to passing children.

### Step 6

Goal: Introduce the concept of re-usable components. It would be really **AWESOME** if creating DOM descriptions could be incorporated into re-usable functions parametrized with data.

we want to create a function that creates an unordered list with a variable number of items.

```js
const ItemsList = (props) => {

}

render( ItemsList({
    items: ['Item 1', 'Item 2']
}), document.getElementById('app'));
```

The resulting DOM should look like:
```html
<ul>
   <li>Item 1</li>
   <li>Item 2</li>
<ul>
```

Please implement the `ItemsList` function.

Make use of:

* `createElement` function
* Built-in Array `map` function to transform the list of `items` into 'li' DOM descriptions

### Step 7

Goal: Add support for function component(`ItemsList` is a function component) to be specified as type and also support custom attributes.

This basically allows us to define custom tags, besides the browser built-in ones `div` etc.

In React these type of components are called `Stateless Functional Components` (SFC).

#### Requirements

1. Modify `createElement` to accept, as `type` param also a function **besides** string

```js
  const domDescription = createElement(ItemsList, { items: ['Item 1', 'Item 2'] }, []);
```


In example above, `domDescription` should be an object that can be given to `render` as param in order to create the REAL DOM.

To achieve that `createElement` should:

* Check if type is a function
* If yes, call the function and pass as params the received props `{ items: ['Item 1', 'Item 2'] }`
* Take the resulting dom description for the custom tag and return it.

2. Add tests
3. Commit the changes.

### Step 8

Goal: Add support for events and event handlers

We can pass it all kinds of attributes to the elements created. We want to extend that with the ability to pass in event handlers to the elements.

```js
const domDescription = createElement('div', {
            onClick: () => console.log('div clicked'),
            name: 'divName'
        }, [
            createElement('span', { onMouseOver: () => console.log('Mouse over') })
        ]
);

render(domDescription, document.getElementById('app'));
```

#### Requirements

1. Modify `render` to properly attach event handlers when creating the DOM:

* Check if `type` is a `string`. If it is it means we are dealing with a built-in tag.
* If yes, then check each attribute to see if it is of type function
* If it is, use `addEventLister` to setup and event on that element. Do **NOT** set the event handler as attribute on the element.
* In `vdom.js` maintain a `WeakMap` that associates a DOM element with the event handler attached. This is needed so we can later remove the event handlers when the DOM elements are(using removeEventListener ).

2. Add tests
3. Commit the changes

### Step 9

Goal: Add support for ES6 class components.

Up until now we've been using functions in order to define *custom* tags.
We want to support also ES6 classes.

#### Requirements

1. Define a base ES6 class called `Component` that other classes can extend from. For the moment the class body can be empty.
2. Each class that extends `Component` must have a method called `render` that returns DOM descriptions.

Example usage:

```js
class ItemsList extends Component {
    render() {
        return (
            createElement('div', null, [
                createElement('span', null, 'Test content')
            ]);
        )
    }
}
```

3. Update `createElement` function from `vdom.js` so that it checks if property `type` is a class extending from `Component` ( hint: use `instanceof` ). 
4. If it is instantiate the class( yes, you can do `new attributes.type;` in JS!! ).
5. Call it's `render` method to get the DOM description
6. Add tests for this new behaviour and commit the changes.

At this point, `createElement` should accept as type param:
1. string
2. function
3. class

In the end, we should be able to do:
```js
    render(
        createElement(ItemsList, { items: ['Item 1'] }),
        document.getElementById('app')
    )
```

### Step 10

Goal: Add support for patching the REAL DOM when VDOM changes

#### Requirements

We would like to know which VDOM was rendered to which DOM container so we can later patch up the DOM container when the VDOM changes.

1. In `vdom.js` define a `WeakMap` ( https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakMap ) that defines mappings between a DOM Node container and the Virtual DOM description.

Example:
```js
render(createElement('div'), document.getElementById('app'))
render(createElement('ul'), document.getElementById('otherApp'))
``` 

The afore-mentioned WeakMap key will be `document.getElementById('app')` while the value will be the dom description object(`createElement('div')`).

If there is another call to `render`, like in the example above we will have another entry in WeakMap associating `document.getElementById('otherApp')` to `createElement('ul')`;

2. Define in `vdom.js` a `patch(oldVDOM, newVDOM, container)` function.
3. In `render` check if the container is empty(eg using `innerHTML`). If it is just create the VDOM and insert it in container.
4. If it isn't, get the old VDOM already rendered in that container from the WeakMap, create the new VDOM and pass them all to `patch` which should modify the real DOM so that it matches the new VDOM.

We will go with the **simplest** implementation of `patch` function:

* Cleanup the container content
* Cleanup the old event handlers that were set
* Create the new virtual DOM and insert it into the container
* Update the WeakMap to reflect the new association between VDOM and container
* Add event handlers

The net affect after this step is that if you call `render` twice on the same element, then it updates the container content:
```js
render(createElement('div'), document.getElementById('app'));
render(createElement('ul'), document.getElementById('app')); 
``` 

The resulting DOM is
```html
<ul></ul>
```

### Step 11

Goal: Add JSX support

Enable one to write
```js
const domDescription = (<ul>
    <li name="test">Content</test>
<ul>);
```
instead of 
```js
    const domDescription = createElement('ul', null, [
        createElement('li', { name: 'test' }, 'Content')
    ]);
```

The 2 are perfectly equivalent. JSX is just a syntax to make it easier to write the above.

### Requirements

1. Configure `jsxPragma` in babel to compile JSX to `createElement` calls. See https://babeljs.io/docs/plugins/transform-react-jsx/ , section 'Via .babelrc (Recommended)'.

In the example config 'dom' should be replaced with 'createElement'. 

2. Re-write the `createElement` calls in tests to use JSX.


# Project 5 - Getting familiar with basic React

## Goal

Get a good grasp of React , props, state and component based design.

## References
Please read-up and follow along the following:

1. https://reactarmory.com/guides/learn-react-by-itself 
2. https://egghead.io/courses/start-learning-react

## Requirements

Create a React application that re-creates the TodoMVC example:

http://todomvc.com/examples/react/#/

Work in a TDD manner at all times( tests first followed by component implementation ).


# Project 6 - Getting functional

## Goal

Prepare the ground for getting to know a super popular state management solution
for React apps.
But in order to use Redux efficiently you need a strong functional base.

# Requirements

Follow the tutorial in https://github.com/timoxley/functional-javascript-workshop 
and resolve all of the exercises.


# Project 7 - Adding a state management layer

Redux is a super popular state management lib for React.
However, before using the real Redux, we will  implement a Redux solution from scratch.

### Requirements

* Create a `state-management.js` file.

1. Declare and implement and ES6 `Store` class having:
* a `state` member - defined in the class. It's initial value is set by constructor.
* a `reducer` member which is a function - received as constructor param
* a `constructor(reducer, initialState)` declaration. `initialState`
* a `dispatch(action)` member( dispatch is a function) - defined in the class
* a `getState()` member, returning the value of the `state` member - defined in the class

Regarding `state` member:
* can only be changed by using the `dispatch` function.
* will be an object( initially empty ).

Regarding `dispatch` member:

* `dispatch(action)` - action is an object that **always** has the format `{ type: 'some_action_name', payload: object }`
* dispatch will pass the current Store `state` and the `action` as params to the `reducer` function in the Store
* `reducer` is expected to do something with the action and current state and return a new state
* the new state returned by the `reducer` becomes the `state` of the Store

2. Write tests verifying that Store can operate around these lines:

```js
const rootReducer = (state, action) => {
    switch(action.type) {
        case 'change_flag':
            return Object.assign(state, {flag: action.payload });
        default:
            return state;
    }
}
const s = new Store(rootReducer, { flag: false });
s.dispatch({ type: "change_flag", payload: true });

// s.getState() should equal { flag: true }

s.dispatch({ type: "change_flag", payload: false });

// s.getState() should equal { flag: false }
```

2. Implement a `createStore(reducer, initialState)` function that creates and returns a `Store` instance. Add tests for it. 

```js
const s = createStore(rootReducer, initialState);
s.dispatch( etc );
s.getState();
```

3. Modify the `Store` class defined previously and allow a way to subscribe to changes. We want to be able to be notified whenever a Store `state` member changes.

* Define a `subscribe(listener)` method on the `Store` class. `listener` is a function.
* Define a member `listeners` of type array that holds all of the listener functions.
* Whenever the state changes( hint: in `dispatch` method ) go over all listeners in the listeners array and call them, passing as argument to each the new `state` of the Store.

```js
const s = createStore(rootReducer, {});
// Setup the listener
s.subscribe((newState) => {
    console.log(newState);
});
// And change the state of the store via dispatch
s.dispatch({ type: 'change_flag', payload: true });

```

4. Add tests for `subscribe` functionality. Make sure that you use `jest.fn()` to test out the  and make assertions regarding how many times the listener was called and using which arguments ( http://facebook.github.io/jest/docs/en/jest-object.html#jestfnimplementation )
 
# Project 8 - Adding the REAL Redux

### Resources

Follow step by step the lessons below:

1. https://egghead.io/courses/getting-started-with-redux
2. https://egghead.io/courses/building-react-applications-with-idiomatic-redux
2. Example app: https://github.com/reactjs/redux/tree/master/examples/todomvc

### Requirements

Re-write the TodoMVC app to use React+Redux.
